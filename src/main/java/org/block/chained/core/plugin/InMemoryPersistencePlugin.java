/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */

package org.block.chained.core.plugin;

import com.rits.cloning.Cloner;
import org.apache.commons.lang3.StringUtils;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.BlockPage;
import org.block.chained.core.model.BlockPageImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.StampedLock;

/**
 * Max entries limited to 2,147,483,647
 *
 * @param <D>
 */
public class InMemoryPersistencePlugin<D extends BlockData> implements PersistencePlugin<D> {

    private final ConcurrentMap<String, BlockEntry<D>> chainData = new ConcurrentHashMap<>();
    private final StampedLock lastHashLock = new StampedLock();
    private String lastHash;
    private final String genesisHash;

    private final Block<D> genesisBlock;

    private static final Cloner CLONER = new Cloner();

    public InMemoryPersistencePlugin(Block<D> genesisBlock) {
        this.genesisHash = genesisBlock.getHash();
        // TODO Throw an exception if genesishash is null or empty
        this.lastHash = this.genesisHash;
        this.chainData.put(this.genesisHash, new BlockEntry<>(genesisBlock));
        this.save(genesisBlock);
        this.genesisBlock = genesisBlock;
    }

    @Override
    public Block<D> getGenesisBlock() {
        return this.genesisBlock;
    }

    @Override
    public Block<D> getLastBlock() {
        long stamp = lastHashLock.readLock();
        try {
            return getBlock(lastHash);
        }finally {
            lastHashLock.unlockRead(stamp);
        }
    }

    @Override
    public Block<D> getBlock(String hash) {
        return chainData.get(hash).getBlock();
    }

    @Override
    public BlockPage<D> getBlocks(String indexHash, int size) {
        List<Block<D>> content = new ArrayList<>(size);
        BlockEntry<D> blockEntry = chainData.get(indexHash);
        // TODO Throw an exception if the block entry is not found
        String nextHash = blockEntry.getNextHash();

        for(int i = 0; i < size; i++){
            content.add(blockEntry.getBlock());
            if(StringUtils.isEmpty(nextHash)){
                break;
            }
            blockEntry = chainData.get(nextHash);
            // TODO What do we do if the block entry is null?
            nextHash = blockEntry.getNextHash();
        }

        BlockPageImpl<D> page = new BlockPageImpl<>();
        page.setContent(content);
        page.setSize(content.size());
        page.setIndex(indexHash);
        return page;
    }

    @Override
    public Block<D> save(Block<D> block) {
        chainData.computeIfAbsent(block.getHash(), s -> {
            long stamp = lastHashLock.writeLock();
            try {
                BlockEntry<D> lastEntry = chainData.get(this.lastHash);
                lastEntry.setNextHash(s);
                this.lastHash = s;
                return new BlockEntry<>(block);
            }finally {
                lastHashLock.unlockWrite(stamp);
            }
        });
        return getBlock(block.getHash());
    }

    /**
     * Blocks are cloned when read adn written from the entry.
     *
     * @param <D>
     */
    private static final class BlockEntry<D extends BlockData> {
        private Block<D> block;
        private String nextHash;

        BlockEntry(Block<D> block) {
            this.block = block;
            this.nextHash = null;
        }


        Block<D> getBlock() {
            return CLONER.deepClone(this.block);
        }

        void setBlock(Block<D> block) {
            this.block = CLONER.deepClone(block);
        }

        String getNextHash() {
            return nextHash;
        }

        void setNextHash(String nextHash) {
            this.nextHash = nextHash;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BlockEntry<?> that = (BlockEntry<?>) o;
            return Objects.equals(block, that.block) &&
                    Objects.equals(nextHash, that.nextHash);
        }

        @Override
        public int hashCode() {
            return Objects.hash(block, nextHash);
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", BlockEntry.class.getSimpleName() + "[", "]")
                    .add("block=" + block)
                    .add("nextHash='" + nextHash + "'")
                    .toString();
        }
    }
}

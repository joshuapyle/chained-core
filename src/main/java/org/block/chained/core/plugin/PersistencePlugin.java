/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.plugin;

import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockPage;

/**
 * Responsible for persisting blocks to memory or disk.  The interface allows an unlimited combination of persistence
 * options.
 *
 * @param <D> The data being stored in the block it must extend the {@link BlockData} interface
 */
public interface PersistencePlugin<D extends BlockData> {

    /**
     * Get the first (genesis) block on the chain.
     *
     * @return the first (genesis) block on the chain
     */
    Block<D> getGenesisBlock();

    /**
     * Get the last block in the chain as it was last persisted.
     *
     * @return the last block in the chain as it was last persisted
     */
    Block<D> getLastBlock();

    /**
     * Return the Block for the given hash.  Implementers are responsible for handling null values.
     *
     * @param hash the hash of the block requested
     *
     * @return the Block that corresponds to the hash
     */
    Block<D> getBlock(String hash);

    /**
     * Get a page/segment of the chain starting at the given indexHash and returning no more than the indicated size.
     * If there are no more blocks the results must return a page with an empty content.
     *
     * Compare the hash of first and last entry hash to the genesis block or the 'last' block to determine if there are
     * more entries in either direction.
     *
     * @param indexHash the hash to start from
     * @param size the max number of blocks to return
     *
     * @return the Page of blocks requested
     */
    BlockPage<D> getBlocks(String indexHash, int size);

    /**
     * Persist the given block
     *
     * @param block the block to persist
     *
     * @return the block after it was persisted
     */
    Block<D> save(Block<D> block);
}

/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.exception;

public class InvalidBlockException extends RuntimeException {
    public InvalidBlockException(String message) {
        super(message);
    }
}

/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import org.block.chained.core.model.BlockData;
import org.block.chained.core.model.Block;

public interface BlockService<D extends BlockData> {

    Block<D> createBlock(String previousHash, D blockData);

    String calculateHash(Block<D> block);

    Boolean validate(Block<D> block, String expectedHash);
}

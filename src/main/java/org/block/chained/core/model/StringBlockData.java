/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.model;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Used to store String data as the "data" stored in each block.  This type is the most basic and flexible BlockData
 * possible.
 */
public final class StringBlockData implements BlockData {

    private String data = "";

    public StringBlockData() {
    }

    public StringBlockData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringBlockData that = (StringBlockData) o;
        return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String stringForHash() {
        return this.data;
    }

    @Override
    public String toString() {
        return data;
    }
}
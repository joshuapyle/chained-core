/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core.model;

public interface BlockData {
    /**
     * A utility method for converting the BlockData to a String.  This permits you to have a custom toString method for
     * viewing and logging the data while having a different method for generating the hash.
     *
     * @return a string to be used when generating the hash
     */
    String stringForHash();
}

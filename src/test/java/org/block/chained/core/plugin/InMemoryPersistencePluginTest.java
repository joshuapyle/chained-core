/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */

package org.block.chained.core.plugin;

import org.block.chained.core.BlockService;
import org.block.chained.core.BlockServiceImpl;
import org.block.chained.core.ChainService;
import org.block.chained.core.ChainServiceImpl;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockPage;
import org.block.chained.core.model.StringBlockData;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * More f
 *
 */
public class InMemoryPersistencePluginTest {

    private ChainService<StringBlockData> chainService;

    private BlockService<StringBlockData> blockService;

    private Block<StringBlockData> genesisBlock;

    @Before
    public void setUp() {
        HashGeneratorPlugin<StringBlockData> hashGeneratorPlugin = new Sha3HashGeneratorPluginImpl<>(SHA3.Digest512.class);
        blockService = new BlockServiceImpl<>(hashGeneratorPlugin);

        genesisBlock = blockService.createBlock("0", new StringBlockData("Genesis Data!"));
        String genesisHash = blockService.calculateHash(genesisBlock);
        genesisBlock.setHash(genesisHash);

        PersistencePlugin<StringBlockData> persistencePlugin = new InMemoryPersistencePlugin<>(genesisBlock);
        chainService = new ChainServiceImpl<>(blockService, persistencePlugin);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getGenesisBlockWithNewChain() {
        Block<StringBlockData> block = chainService.getGenesisBlock();
        assertThat(block, is(equalTo(this.genesisBlock)));
    }

    @Test
    public void getLastBlockWithNewChain() {
        Block<StringBlockData> block = chainService.getLastBlock();
        assertThat(block, is(equalTo(this.genesisBlock)));
    }

    @Test
    public void getBlockWithNewChain() {
        Block<StringBlockData> block = chainService.getBlock(this.genesisBlock.getHash());
        assertThat(block, is(equalTo(this.genesisBlock)));
    }

    @Test
    public void getBlocksWithNewChain() {
        BlockPage<StringBlockData> blockPage = chainService.getBlocks(this.genesisBlock.getHash(), 10);
        assertThat(blockPage, is(notNullValue()));
        assertThat(blockPage.getContent(), is(not(empty())));
        assertThat(blockPage.getContent(), hasSize(1));

    }

    @Test
    public void saveWithNewChain() {
        String lastHash = chainService.getLastBlock().getHash();

        Block<StringBlockData> block = blockService.createBlock(lastHash, new StringBlockData("First Block in saveWithNewChain"));
        chainService.addBlock(block);

        BlockPage<StringBlockData> blockPage = chainService.getBlocks(this.genesisBlock.getHash(), 10);
        assertThat(blockPage, is(notNullValue()));
        assertThat(blockPage.getContent(), is(not(empty())));
        assertThat(blockPage.getContent(), hasSize(2));

        Block<StringBlockData> genesisBlock = chainService.getGenesisBlock();
        Block<StringBlockData> lastBlock = chainService.getLastBlock();
        assertThat(lastBlock, is(not(equalTo(genesisBlock))));
        assertThat(lastBlock, is(equalTo(block)));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void save9WithNewChain() {
        String dataFormat = "Block#%d in save9WithNewChain";

        Block<StringBlockData> block = null;
        List<Block<StringBlockData>> blocks = new ArrayList<>();
        //blocks.add(genesisBlock);
        for(int i = 0; i < 9; i++){
            String lastHash = chainService.getLastBlock().getHash();
            block = blockService.createBlock(lastHash, new StringBlockData(String.format(dataFormat,i)));
            block = chainService.addBlock(block);
            blocks.add(block);
        }

        BlockPage<StringBlockData> blockPage = chainService.getBlocks(this.genesisBlock.getHash(), 10);
        assertThat(blockPage, is(notNullValue()));
        assertThat(blockPage.getContent(), is(not(empty())));
        assertThat(blockPage.getContent(), hasSize(10));
        assertThat(blockPage.getContent(), hasItem(genesisBlock));
        System.out.println("genesisBlock = " + genesisBlock);
        assertThat(blockPage.getContent(), hasItems(blocks.toArray(new Block[0])));


        Block<StringBlockData> genesisBlock = chainService.getGenesisBlock();
        Block<StringBlockData> lastBlock = chainService.getLastBlock();
        assertThat(lastBlock, is(not(equalTo(genesisBlock))));
        assertThat(lastBlock, is(equalTo(block)));
    }
}
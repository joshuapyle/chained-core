/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import org.block.chained.core.model.Block;
import org.block.chained.core.model.BlockPage;
import org.block.chained.core.model.BlockPageImpl;
import org.block.chained.core.model.StringBlockData;
import org.block.chained.core.plugin.PersistencePlugin;
import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class ChainServiceImplTest {
    private ChainService<StringBlockData> chainService;

    @Mock
    private BlockService<StringBlockData> mockBlockService;

    @Mock
    private PersistencePlugin<StringBlockData> mockPersistencePlugin;

    private static final String GENESIS_HASH = "d7f66597799e6a5a9b2e59dcc062025cb6b717e406b287e40642f6254c37589d0ace0e60f36009ef57b504f25fc99a55e54ee4c12f9ea552f8ce503ce6cac053";
    private static final String LAST_HASH = "65c7709a1134e37bd8417e1bb236109c0e8b13075a05e23883baea4bd3e8a0a61775a6d6390caeea92ccf34894d0c5ae95ee7f4ff0d81b1f9323b53936341b3e";
    private static final Block<StringBlockData> GENESIS_BLOCK;
    private static final Block<StringBlockData> LAST_BLOCK;

    static {
        GENESIS_BLOCK = new Block<>();
        GENESIS_BLOCK.setPreviousHash("0");
        GENESIS_BLOCK.setData(new StringBlockData("Genesis"));
        GENESIS_BLOCK.setHash(GENESIS_HASH);

        LAST_BLOCK = new Block<>();
        LAST_BLOCK.setPreviousHash(GENESIS_HASH);
        LAST_BLOCK.setData(new StringBlockData("Junk Data"));
        LAST_BLOCK.setHash(LAST_HASH);
    }

    @Before
    @SuppressWarnings("unchecked")
    public void before() {
        reset(mockBlockService);
        reset(mockPersistencePlugin);

        // Genesis Block Setup
        doReturn(GENESIS_BLOCK).when(mockPersistencePlugin).getBlock(GENESIS_HASH);
        doReturn(GENESIS_BLOCK).when(mockPersistencePlugin).getGenesisBlock();

        // Last Block Setup
        doReturn(LAST_BLOCK).when(mockPersistencePlugin).getBlock(LAST_HASH);
        doReturn(LAST_BLOCK).when(mockPersistencePlugin).getLastBlock();

        doAnswer((Answer<String>) invocation -> {
            Block<StringBlockData> block = invocation.getArgument(0);
            return calculateHash(block.getPreviousHash(), block.getData().stringForHash());
        }).when(mockBlockService).calculateHash(ArgumentMatchers.any());

        this.chainService = new ChainServiceImpl<>(mockBlockService, mockPersistencePlugin);
    }

    private String calculateHash(String previousHash, String data) {
        String input = previousHash + ":" + data;
        SHA3.DigestSHA3 sha3 = new SHA3.Digest512();
        sha3.update(input.getBytes());
        return Hex.toHexString(sha3.digest());
    }

    @After
    public void after(){
        verifyNoMoreInteractions(mockBlockService);
        verifyNoMoreInteractions(mockPersistencePlugin);
    }

    @Test
    public void getGenesisBlock() {
        Block<StringBlockData> genesisBlock = chainService.getGenesisBlock();
        assertThat(genesisBlock, is(notNullValue()));
        verify(mockPersistencePlugin, times(1)).getGenesisBlock();
    }

    @Test
    public void getLastBlock() {
        Block<StringBlockData> lastBlock = chainService.getLastBlock();
        assertThat(lastBlock, equalTo(LAST_BLOCK));
        verify(mockPersistencePlugin, times(1)).getLastBlock();
    }

    @Test
    public void addBlock() {
        StringBlockData newBlockData = new StringBlockData("New Block Data!");
        Block<StringBlockData> newBlock = new Block<>();
        newBlock.setData(newBlockData);

        doAnswer(
                (Answer<Block<StringBlockData>>) invocation -> invocation.getArgument(0)
        ).when(mockPersistencePlugin).save(ArgumentMatchers.any());

        Block<StringBlockData> addedBlock = chainService.addBlock(newBlock);
        assertThat(addedBlock, is(notNullValue()));
        assertThat(addedBlock.getHash(), is(notNullValue()));
        assertThat(addedBlock.getHash(), equalTo("10f1a7a701f96c150e0c54bdad434edfcc2acc1672e7646670eaf317c615e9c2f866f54bbb79401314fdb26bb4800c82c70080bcc4e6f7b788f873b6be47eb68"));
        assertThat(addedBlock.getPreviousHash(), equalTo(LAST_HASH));
        assertThat(addedBlock.getData(), equalTo(newBlockData));

        verify(mockPersistencePlugin, times(1)).save(ArgumentMatchers.any());
        verify(mockBlockService, times(1)).calculateHash(ArgumentMatchers.any());

        // TODO last block should now be the addedBlock

    }

    @Test
    public void getBlockGenesis() {
        Block<StringBlockData> block = chainService.getBlock(GENESIS_HASH);
        assertThat(block, is(notNullValue()));
        assertThat(block, equalTo(GENESIS_BLOCK));

        verify(mockPersistencePlugin, times(1)).getBlock(GENESIS_HASH);
    }

    @Test
    public void getBlockLast() {
        Block<StringBlockData> block = chainService.getBlock(LAST_HASH);
        assertThat(block, is(notNullValue()));
        assertThat(block, equalTo(LAST_BLOCK));

        verify(mockPersistencePlugin, times(1)).getBlock(LAST_HASH);
    }

    @Test
    public void getBlocks() {
        doAnswer((Answer<BlockPage<StringBlockData>>) invocation -> {
            BlockPageImpl<StringBlockData> blockPage = new BlockPageImpl<>();
            blockPage.setSize(invocation.getArgument(1));
            List<Block<StringBlockData>> content = new ArrayList<>();
            content.add(GENESIS_BLOCK);
            content.add(LAST_BLOCK);
            blockPage.setContent(content);
            return blockPage;
        }).when(mockPersistencePlugin).getBlocks(GENESIS_HASH, 10);

        BlockPage<StringBlockData> page = chainService.getBlocks(GENESIS_HASH, 10);
        assertThat(page, is(notNullValue()));
        assertThat(page.getSize(), equalTo(10));
        assertThat(page.getContent(), is(notNullValue()));
        assertThat(page.getContent(), hasSize(2));
    }

    @Test
    public void validateChainWithTwoEntries() {
        boolean valid = chainService.validateChain();
        assertThat(valid, equalTo(true));

        verify(mockBlockService, times(0)).calculateHash(ArgumentMatchers.any());
    }

    @Test
    public void validateChainWithThreeEntries() {
        StringBlockData newBlockData = new StringBlockData("New Block Data!");
        Block<StringBlockData> newBlock = new Block<>();
        newBlock.setData(newBlockData);

        doAnswer(
            (Answer<Block<StringBlockData>>) invocation -> invocation.getArgument(0)
        ).when(mockPersistencePlugin).save(ArgumentMatchers.any());

        chainService.addBlock(newBlock);
        chainService.getLastBlock();

        boolean valid = chainService.validateChain();
        assertThat(valid, equalTo(true));

        verify(mockBlockService, times(2)).calculateHash(ArgumentMatchers.any());
    }

    @Test
    public void lengthWithTwoEntries() {
        long length = chainService.length();
        assertThat(length, equalTo(2L));

        verify(mockBlockService, times(0)).calculateHash(ArgumentMatchers.any());
    }

    @Test
    public void lengthWithThreeEntries() {
        StringBlockData newBlockData = new StringBlockData("New Block Data!");
        Block<StringBlockData> newBlock = new Block<>();
        newBlock.setData(newBlockData);

        doAnswer(
                (Answer<Block<StringBlockData>>) invocation -> invocation.getArgument(0)
        ).when(mockPersistencePlugin).save(ArgumentMatchers.any());

        chainService.addBlock(newBlock);
        chainService.getLastBlock();

        long length = chainService.length();
        assertThat(length, equalTo(3L));

        verify(mockBlockService, times(1)).calculateHash(ArgumentMatchers.any());
    }
}
/*
 * Copyright (c) 2018 , Joshua T. Pyle and the block-chained contributors.
 */
package org.block.chained.core;

import org.block.chained.core.exception.InvalidBlockException;
import org.block.chained.core.model.Block;
import org.block.chained.core.model.StringBlockData;
import org.block.chained.core.plugin.HashGeneratorPlugin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class BlockServiceImplTest {

    private static final String LARGE_DATA = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla et dui dictum congue. Etiam vitae lacinia lectus. Nulla interdum quam purus, sit amet molestie tortor efficitur sed. Fusce vulputate luctus ante, at fermentum lacus tempus eget. Fusce nibh purus, tempus sit amet quam quis, feugiat blandit odio. Mauris non lacus sed metus tincidunt accumsan ac eu libero. Proin elementum nisi placerat felis fringilla porttitor. Maecenas scelerisque, orci sed porttitor tristique, nisi erat mattis diam, in posuere metus diam at nibh. Mauris semper semper ornare. Ut efficitur, enim a mollis sodales, orci leo congue eros, a imperdiet tellus velit sit amet lorem. Proin feugiat dolor sit amet dignissim imperdiet. Ut scelerisque, tortor pellentesque viverra pharetra, lorem sapien pharetra lacus, eu bibendum ex odio sit amet nisi. Nullam bibendum tristique sapien, sed tempor ligula fermentum et. Cras sollicitudin rutrum turpis ut congue. Curabitur sit amet felis vitae enim volutpat.";
    private static final String STARTING_HASH = "e5fd12a2be922061a8ea399badd7f57603f276557c6b0a8b0013ff3e138748a805a048c234e65c42ad945fddb9b46ee8579fc0a240b5542362192d9c4d053628";

    private BlockService<StringBlockData> blockService;

    @Mock
    private HashGeneratorPlugin<StringBlockData> mockHashGeneratorPlugin;

    @Before
    @SuppressWarnings("unchecked")
    public void before(){
        reset(mockHashGeneratorPlugin);
        blockService = new BlockServiceImpl<>(mockHashGeneratorPlugin);
    }

    @After
    public void after(){
        verifyNoMoreInteractions(mockHashGeneratorPlugin);
    }

    @Test
    public void createBlock() {
        String hashString = "6f29280004d4f21b9d6e9eef195ab2b32de573d524752860129c09fe864c394506bfba8069b4fb0480092e717b41fe56d60494b677f780bbfdb10abc9a06ff51";
        when(mockHashGeneratorPlugin.calculateHash(ArgumentMatchers.any())).thenReturn(hashString);

        Block<StringBlockData> block = blockService.createBlock(STARTING_HASH, new StringBlockData(LARGE_DATA));

        Block<StringBlockData> expectedBlock = new Block<>();
        expectedBlock.setHash(hashString);
        expectedBlock.setPreviousHash(STARTING_HASH);
        expectedBlock.setData(new StringBlockData(LARGE_DATA));
        assertThat(expectedBlock, equalTo(block));
    }

    @Test(expected = InvalidBlockException.class)
    public void createBlockNullBlockData() {
        try {
            blockService.createBlock(STARTING_HASH, null);
        }catch (InvalidBlockException ibe){
            assertThat(ibe.getMessage(), containsString("data cannot be null or empty"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(null);
            throw ibe;
        }
    }

    @Test(expected = InvalidBlockException.class)
    public void createBlockNullPreviousHash() {
        try {
            blockService.createBlock(null, new StringBlockData(LARGE_DATA));
        }catch (InvalidBlockException ibe){
            assertThat(ibe.getMessage(), containsString("must have a previousHash"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(null);
            throw ibe;
        }
    }

    @Test
    public void calculateHash() {
        String hashString = "6f29280004d4f21b9d6e9eef195ab2b32de573d524752860129c09fe864c394506bfba8069b4fb0480092e717b41fe56d60494b677f780bbfdb10abc9a06ff51";
        when(mockHashGeneratorPlugin.calculateHash(ArgumentMatchers.any())).thenReturn(hashString);

        Block<StringBlockData> block = new Block<>();
        block.setHash(hashString);
        block.setPreviousHash(STARTING_HASH);
        block.setData(new StringBlockData(LARGE_DATA));

        String hash = blockService.calculateHash(block);
        assertThat(hash, equalTo(hashString));
        verify(mockHashGeneratorPlugin, times(1)).calculateHash(block);
    }

    @Test(expected = InvalidBlockException.class)
    public void calculateHashNullBlock() {
        try {
            blockService.calculateHash(null);
        }catch (InvalidBlockException ibe){
            assertThat(ibe.getMessage(), containsString("block cannot be null"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(null);
            throw ibe;
        }
    }

    @Test
    public void calculateHashNullHash() {
        Block<StringBlockData> block = new Block<>();
        block.setHash(null);
        block.setPreviousHash(STARTING_HASH);
        block.setData(new StringBlockData(LARGE_DATA));

        when(mockHashGeneratorPlugin.calculateHash(block)).thenReturn("JUNK STRING");

        String hashString = blockService.calculateHash(block);
        assertThat(hashString, equalTo("JUNK STRING"));
        verify(mockHashGeneratorPlugin, times(1)).calculateHash(block);
    }

    @Test(expected = InvalidBlockException.class)
    public void calculateHashNullData() {
        Block<StringBlockData> block = new Block<>();
        block.setPreviousHash(STARTING_HASH);
        block.setData(null);

        try {
            blockService.calculateHash(block);
        }catch (InvalidBlockException ibe){
            assertThat(ibe.getMessage(), containsString("data cannot be null or empty"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(block);
            throw ibe;
        }
    }

    @Test(expected = InvalidBlockException.class)
    public void calculateHashNullPreviousHash() {
        Block<StringBlockData> block = new Block<>();
        block.setPreviousHash(null);
        block.setData(new StringBlockData(LARGE_DATA));

        try {
            blockService.calculateHash(block);
        }catch (InvalidBlockException ibe){
            assertThat(ibe.getMessage(), containsString("must have a previousHash"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(block);
            throw ibe;
        }
    }

    @Test
    public void validate() {
        Block<StringBlockData> block = new Block<>();
        block.setHash(null);
        block.setPreviousHash(STARTING_HASH);
        block.setData(new StringBlockData(LARGE_DATA));

        when(mockHashGeneratorPlugin.calculateHash(block)).thenReturn("JUNK STRING");

        boolean valid = blockService.validate(block, "JUNK STRING");
        assertThat(valid, equalTo(true));
    }

    @Test(expected = RuntimeException.class)
    public void validateNullBlock() {
        try {
            blockService.validate(null, null);
        }catch (RuntimeException rte){
            assertThat(rte.getMessage(), containsString("expectedHash cannot be null"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(null);
            throw rte;
        }
    }

    @Test(expected = RuntimeException.class)
    public void validateNullExpectedHash() {
        Block<StringBlockData> block = new Block<>();
        block.setHash(null);
        block.setPreviousHash(STARTING_HASH);
        block.setData(new StringBlockData(LARGE_DATA));

        try {
            blockService.validate(block, null);
        }catch (RuntimeException rte){
            assertThat(rte.getMessage(), containsString("expectedHash cannot be null"));
            verify(mockHashGeneratorPlugin, times(0)).calculateHash(block);
            throw rte;
        }
    }
}